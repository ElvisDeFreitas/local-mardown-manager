(function(){

var legenda = {
	resolvido: "#DBFCCF",
	subir: "#CAEEFA",
	pendente: "#FADEEB",
	pendenteAlguem: "#F8FFC9"
};

$(function(){
 $.fn.regexContains = function(pattern, fn, fn_a){
    var fn = fn || $.fn.text;
    return this.filter(function() {
      return pattern.test(fn.apply($(this), fn_a));
    });
  };
	// colorindo as legendas
	$(".resolvido").css({background: legenda.resolvido});
	$(".pendente").css({background: legenda.pendente});
	$(".pendenteAlguem").css({background: legenda.pendenteAlguem});
	$(".subir").css({background: legenda.subir});

	// colorindo o sistema
	legenda.eResolvido = getResolvido().css({background: legenda.resolvido});

	legenda.eSubir  = getSubir().css({background: legenda.subir});

	legenda.ePendente = getPendente().css({background: legenda.pendente});
	
	legenda.ePendenteAlguem = getPendenteAlguem().css({background: legenda.pendenteAlguem});

});

function getPendente(){
	return getAllPendente()
		.not(legenda.eSubir).not(legenda.eResolvido);
}
function getAllPendente(){
	return $("li:contains('[PEDING'), li:contains('[]'), li:contains('[ ]')");
}
function getSubir(){
	return getAllSubir()
		.not(legenda.eResolvido);
}
function getAllSubir(){
	return $("li:contains('[SUBIR'), li:contains('[FAZER-TESTE-UNITARIO'), li:contains('[IMP-AGUARDAR')");
}
function getResolvido(){
	return getAllResolvido()
		.not(getPendente()).not(getSubir());
}
function getAllResolvido(){
	return $("li:contains('[OK'),li:contains('[DEF-OK'), li:contains('[IMP-OK')");
}
function getPendenteAlguem(){
	return getAllPendenteAlguem()
		.not(legenda.eResolvido).not(legenda.ePendente);
}
function getAllPendenteAlguem(){
	return $("li:contains('[DEF'), li:contains('[IMP')");
}

})();