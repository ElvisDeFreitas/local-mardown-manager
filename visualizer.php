<?php
include "autoloader.php";

use \Michelf\MarkdownExtra;

$fileToOpen = $_GET['file'];
$confFile = $_GET['conf'];

if(!$fileToOpen){
	echo "<h1>Por favor passe um arqui em 'file'</h1>";
	return -1;
}
if(!file_exists($fileToOpen)){
    echo ("<h1>O arquivo $fileToOpen, não existe<h1>"); 
    exit();
}

$fileName = basename($fileToOpen);

$fileParts = pathinfo($fileToOpen);
if($fileParts['extension'] == 'php'){
    ob_start();
    require $fileToOpen;
    $originalText = ob_get_clean();
        
}else{
    $originalText = file_get_contents($fileToOpen);
}


?>

<link rel="stylesheet" href="script/github-markdown.css" />
<link rel="stylesheet" href="script/prism.css" />
<link rel="stylesheet" href="script/estilo.css" />
<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="script/prism.js"></script>
<script src="script/script.js"></script>
<meta charset="utf-8">

<ul class="legenda">
	<li class="pendente">PENDENTE EU</li>
	<li class="pendenteAlguem">PENDENTE ALGUÉM</li>
	<li class="subir">PARCIALMENTE RESOLVIDO</li>
	<li class="resolvido">RESOLVIDO</li>
</ul>

<?php
    echo "<title>$fileName - $fileToOpen</title>";
    // chamando configuração extra
    if($confFile){
        require $confFile;
    }
?>
<div class="markdown-body">

<?php
$parser = new MarkdownExtra();
$parser->code_class_prefix = "language-";
$html = $parser->transform($originalText);
echo $html;

?>
</div>
